from setuptools import setup

setup(
        name='LoggingSystem',
        packages=['LoggingSystem'],
        version='0.1.1',
        include_package_data=True,
        description="Document Logging System For the Adamson University Science Department.",
        zip_safe=False,
        install_requires=[
            'flask',
            'Flask-WTF',
            'WTforms',
            'flask-login',
            'flask_sqlalchemy',
            'flask_bcrypt',
            'pyodbc', # For MSSQL Databases
            'pymysql'
            ]
        )
