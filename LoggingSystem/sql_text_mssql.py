##
##	CREATE
##


def create_code():
	text='''
		insert into [file] (code, name, deleted)
		values (:code, :name, 0)
	'''
	return text

def create_document():
	text= ''' 
		insert into document (code, subject, deleted, sent_by, received_by, doc_image_name)
		values (:code_id, :subject, 0, :sb, :rb, :doc_image_name)
		
	'''
	return text

def create_record():
	text = '''
		insert into record (file_id, document_id,comm_type, datetime, processed_by, deleted)
		values (:code_id, :doc_id, :comm_type, :date, :processed_by, 0)
	'''
	return text
##
## DELETE
##

def soft_delete_codes():
	text= '''
		update [file] set deleted=1
		where code like :code
	'''
	return text

def soft_delete_records():
	text=''' 
		update record set deleted=1
		where rec_id like :rec_id
		update document set deleted=1
		where doc_id like :doc_id
	'''
	return text

def delete_code():
	text= '''
		delete from [file]
		where code like :code
	'''
	return text

def delete_records():
	text='''
		delete from record
		where rec_id like :rec_id
		delete from document
		where doc_id like :doc_id
	'''
	return text

##
## GET
##

def get_file_by_code():
	text = '''
		select * 
		from [file]
		where code like :code
		and deleted=0
	'''
	return text

def get_record_by_id():
	text='''
		select * 
		from record
		where rec_id like :rec_id and
		deleted=0
	'''
	return text

def get_document_by_id():
	text='''
		select * 
		from document
		where doc_id like :rec_id and
		deleted=0
	'''
	return text

def get_latest_document():
	text='''
		select *
		from document
		where doc_id=(select max(doc_id) from document) and
		deleted=0
	'''
	return text
def get_count():
	text='''
		select count(*) from document
		where deleted=0
	'''
	return text


def get_earliest_date():
	return '''
		select min(datetime) from record
		where deleted=0
	'''

def get_latest_date():
	return '''
		select max(datetime) from record
		where deleted=0
	'''


def get_codes():
	text='''
		select * 
		from [file]
		where deleted=0
		order by code asc
	'''
	return text



##
## Joins
##
def get_all_records(search_type):
	if search_type == 'all':
		search = ''' (
			[file].code like :query or document.subject like :query or document.sent_by like :query or document.received_by like :query or
			[file].name like :query or record.processed_by like :query or record.comm_type like :query) '''
	elif search_type == 'subject':
		search = '''(document.subject like :query)'''
	elif search_type == 'file_code':
		search = '''([file].code like :query)'''
	elif search_type == 'sender':
		search = '''(document.sent_by like :query)'''
	elif search_type == 'receiver':
		search = '''(document.received_by like :query)'''
	elif search_type == 'processed_by':
		search = '''(record.processed_by like :query)'''
	elif search_type == 'comm':
		search = '''([file].name like :query)'''
	text='''
		select [file].name, [file].code , document.sent_by, document.received_by, document.subject, record.processed_by, record.comm_type, record.rec_id, record.datetime, document.doc_image_name
		from [file]
		inner join document
			on [file].file_id=document.code
		inner join record
			on record.document_id=document.doc_id

		where'''+search+''' and
			(record.datetime between cast(:date01 as DATETIME2)  and cast(:date02 as DATETIME2)) and
			(record.comm_type in (:rec, :sen)) and
			record.deleted=Convert(Bit,:deleted)
			order by
				case when :ord_by = 'desc' then record.datetime end desc,
				case when :ord_by = 'asc' then record.datetime end asc
			offset :offset rows
			fetch next :per_page rows only

	'''
	return text

##
## UPDATE
##
def update_file():
	text='''
		update [file] set code=:new_code, name=:name
		where code = :code and deleted=0
	'''
	return text
def update_record():
	text='''
		update record set comm_type=:comm_type, datetime=:datetime, processed_by=:processed_by
		where rec_id = :rec_id
	'''
	return text

def update_document():
	text='''
		update document set code=:code, subject=:subject
		where doc_id = :id
	'''
	return text
##
## MISC
##




