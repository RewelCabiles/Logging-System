$(function(){
      'use strict';
      var options = {
        prefetch: true,
        scroll: true,
        cacheLength: 1,
        onStart: {
          duration: 105, // Duration of our animation, if animations are longer than this, it causes problems
          render: function ($container) {

            // Add your CSS animation reversing class
            $container.addClass('is-exiting');  
            // Restart your animation. 
            smoothState.restartCSSAnimations();
          }
        },
        onReady: {
          duration: 50,
          render: function ($container, $newContent) {
            // Remove your CSS animation reversing class
            $container.removeClass('is-exiting');
            // Inject the new content
            $container.html($newContent);
    
          }
        }
      },
      smoothState = $('.m-scene').smoothState(options).data('smoothState');
    });