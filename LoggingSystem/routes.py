from flask import render_template, url_for, flash, redirect, request, send_from_directory
from flask_login import login_user, logout_user, current_user, login_required
from LoggingSystem import app,db, bcrypt 
from LoggingSystem.models import User, File, Document, Record, Permission, Image
from LoggingSystem.helpers import row2dict, Pagination, ScanAndFixSameName
from LoggingSystem.forms import RegisterationForm, LoginForm,AddNewRecordForm, AddNewCodeForm, ChangePasswordForm
from datetime import datetime, timedelta
from werkzeug.utils import secure_filename
import os
import re
import glob
if app.db_type == "mssql":
	from LoggingSystem import sql_text_mssql as sql_text
else:
	from LoggingSystem import sql_text_mysql as sql_text
#hello

@app.route('/view_logs/uploaded/<path:filename>')
@login_required
def get_file(filename):
	print(os.path.join(app.instance_path, 'uploads', filename))
	return send_from_directory(os.path.join(app.instance_path, 'uploads'),filename)

@app.route("/") 	# When the user inputs this route at the end of the url or a link brings them to this URL,
@app.route("/view_logs") # This function/view/page will be called. Also we can add as much routes as we want
@login_required	 	# This Signifies that a login is required. When an anonymous user attempts to enter a URL with this decorator, it will just bring them to the login page.
def view_logs():	
	# This is the default page, and is where the users view the logs
	#===============
	# The block of code below Gets the arguments from the url. For example
	# http://rewelcabiles.pythonanywhere.com/?query=Hello&pp=10&sort_by=id&ord_by=desc
	# This part here is the URL arguments ----->     ?query=Hello&pp=10&sort_by=id&ord_by=desc
	# Its created by the search form on the page.

	print(app.db_type)
	queries = request.args.get('q', '', type=str) 		# This puts the query between '% %' in preperation for the SQL code
	page 	= request.args.get('page', 1, type=int)				# The current page of the search view
	per_page= request.args.get('pp', 10, type=int)				# How many records to show per page
	# Sort By / Order By =======
	sort_by = request.args.get('sb', 'all', type=str)		# What to sort the table by
	ord_by  = request.args.get('ob', 'desc', type=str)		# Sort it in Ascending or Descending?
	#==================================
	# SHOW TYPE======================
	show_type = request.args.get('sr', 'both',type=str)
	filter_records = []
	if show_type == "both":
		sent = 'sent'
		rec  = 'received'
	elif show_type == "sent":
		sent = 'sent'
		rec  = ''
	elif show_type == "rec":
		sent = ''
		rec  = 'received'
	#==================================
	# DATES ==========================
	earliest = db.session.execute(sql_text.get_earliest_date()).fetchone() # Gets records with the earliest/ latest date
	latest = db.session.execute(sql_text.get_latest_date()).fetchone()
	
	# Gets the current time. The utcnow() function gets the date time in UTC +0  Time Format
	# The Philippines timezone is +8 from UTC so we add 8 to the UTCnow date
	current_time = datetime.utcnow() + timedelta(hours=8) 

	# The following two codes are called Ternary operators. With the format : var1 = result1 if condition1 is True else result2
	# In this case, it means that If the earliest/latest dates we got earlier is None/Null (Meaning there are none in the database and that
	# No record has been added yet) Then just use the current date and time
	earliest = current_time if earliest[0] == None else earliest[0] + timedelta(hours=8)
	latest = current_time if latest[0] == None else latest[0] + timedelta(hours=8)

	# This gets the before/after dates from the from the search bar. Ifthere are none, use the earliest/dates we got from earlier as default
	before = request.args.get('db', latest.strftime('%Y-%m-%d'))
	after = request.args.get('da', earliest.strftime('%Y-%m-%d'))
	try :
		before = datetime.strptime(before+' 23:59:00','%Y-%m-%d %H:%M:%S')
		after = datetime.strptime(after+' 00:00:00','%Y-%m-%d %H:%M:%S')
	except: 
		before = latest.strftime('%Y-%m-%d')
		after = earliest.strftime('%Y-%m-%d')
		before = datetime.strptime(before+' 23:59:00','%Y-%m-%d %H:%M:%S')
		after = datetime.strptime(after+' 00:00:00','%Y-%m-%d %H:%M:%S')
		flash("Date Arguement Invalid. Try Reseting Search Options Before continuing", "warning")
	dates=(before, after)
	if dates[0] < dates[1]:
		flash("Filter Options Error, The first date must be older than the second", 'warning')
		dates=(latest, earliest)
	#==================================
	# URL ARGS Validation
	if per_page <= 0:
		per_page = 1 
	#==================================

	record_count=db.session.execute(sql_text.get_count(),{		# This gets the total count of records in the records table
		'table_name': 'records'									# To be used for pagination purposes
		}).fetchone()
	record_count = record_count[0]

	query = sql_text.get_all_records(sort_by)

	posts=db.session.execute(query,{	 # This executes the sql_query. 
		'query': "%"+queries+"%",
		'date01': (dates[1]+ timedelta(hours=-8)).strftime("%Y-%m-%d 00:00"),
		'date02': (dates[0]+ timedelta(hours=-8)).strftime("%Y-%m-%d 23:59"),
		'sort_by':sort_by,
		'ord_by':ord_by,
		'sen':sent,
		'rec':rec,
		'offset':per_page*(page-1),
		'per_page':per_page,
		'deleted':0
		}).fetchall() # Fetchall() means fetch all records that the query outputs. Theres also a function called fetchone() that fetches only the first.
	#==Converting the output of the query into a dictionary (For easy use in the HTML templates)
	all_posts = []	
	for post in posts:
		if post != None:
			post = dict(post)
			record = Record.query.filter_by(rec_id=post["rec_id"]).one()
			print(record.images)

			post['datetime'] = post['datetime'] + timedelta(hours=8)
			post['datetime'] = post['datetime'].strftime("%B %d, %Y %I:%M%p")



			all_posts.append(post)
	#==================================
	open_search = False if queries == '' else True
	print(open_search)
	pagination = Pagination(page, per_page, record_count)		# Helper class to help with table page navigation
	return render_template('log_viewing.html',					# Finally, it renders the template and includes the arguments
		post=all_posts,											# The arguments entered here can be called inside the HTML templates later on
		pp=per_page,
		page=page,
		sb=sort_by,
		ob=ord_by,
		record_count=record_count,
		pagination= pagination,
		show_type=show_type,
		dates=dates,
		q=queries,
		open_search = open_search)



@app.route("/generate_reports")
@login_required
def generate_reports():
	current_time = datetime.utcnow()
	display_current_time = current_time+timedelta(hours=8)
	date = request.args.get('report_date', display_current_time.strftime('%Y-%m-%d'))
	print(date)
	date_object = datetime.strptime(date, '%Y-%m-%d')
	posts = db.session.execute(sql_text.get_records_by_date(), {
		'date01': date_object.strftime("%Y-%m-%d 00:00"),
		'date02': date_object.strftime("%Y-%m-%d 23:59"),
		}).fetchall()

	all_post=[]
	for post in posts:
		if post != None:
			post = dict(post)
			post['datetime'] = post['datetime'] + timedelta(hours=8)
			post['datetime'] = post['datetime'].strftime("%B %d, %Y %I:%M%p")
			all_post.append(post)

	return render_template('create_report.html', posts=all_post, date=date)


@app.route("/preview_document")
@login_required
def preview_document():
	image_name = request.args.get("image_name")
	print(image_name)
	return render_template('image_page.html', image_name=image_name)


@app.route("/view_logs/<rec_id>/delete", methods=['POST'])	# POST means this page will only receive posts and will not actually display a page.
@login_required															
def log_delete(rec_id):
	record = db.session.execute(sql_text.get_record_by_id(), {'rec_id':rec_id}).fetchone()
	db.session.execute(sql_text.soft_delete_records(), {
		'rec_id':record.rec_id,
		'doc_id':record.document_id
		})
	db.session.commit()
	flash('Succesfully Archived Record!', 'info') # Flash creates a popup at the top of a page, the contents of which we can add here as well as the class.
																   # In this case, the class we use is a bootstrap class called info. Which makes the popup blue
	return redirect(url_for('view_logs'))	# Redirect redirects the user into a different view. View in this case, is the function name tied to a page/route


@app.route("/view_logs/manage_file_codes/<code_id>/delete", methods=['POST'])	# POST means this page will only receive posts and will not actually display a page.
@login_required			
def code_delete(code_id):
	print("+==================")
	file = db.session.execute(sql_text.get_file_by_code(), {'code':code_id}).fetchone()
	db.session.execute(sql_text.soft_delete_codes(), {'code':file.code})
	db.session.commit()
	flash("Deleted File with Code: {0}".format(code_id), 'success')
	return redirect(url_for('manage_codes'))



@app.route("/view_logs/manage_file_codes", methods=['GET', 'POST']) # Manage File Codes Page. [Get and Post] means that this page will both send and receive forms
@login_required
def manage_codes():
	all_codes = db.session.execute(sql_text.get_codes()).fetchall() # This gets all the File codes from the database
	print(all_codes)
	form = AddNewCodeForm()	#Forms(Which can be found in forms.py) to be used in the pages forms. 
	if form.validate_on_submit(): # This statement is True if the user posts the form to the server, and the form is validated.
		has_code = db.session.execute(sql_text.get_file_by_code(),{ #Checks to see if the code entered by the user is already in the database
			'code': form.code.data
			}).fetchone()
		
		if has_code == None: # If the code is new, then continue with the new File Code Creation
			query = sql_text.create_code()
			db.session.execute(query, {'code':re.sub('\s+',' ',form.code.data).strip(), 
				'name':form.name.data})
			db.session.commit()
			flash("Added New Code Entry!", 'success')
			return redirect(url_for('manage_codes'))
		else: # Else Flash the message
			flash("That code is already being used, please use a different code", 
				'warning')
	# If he user isn't posting a form, then it means that they're just viewing the page, in which case
	# It just renders the template of the page
	return render_template('file_codes.html', 
		codes=all_codes, 
		title="Manage Codes", 
		form=form ) 


@app.route("/view_logs/new_record", methods=['GET', 'POST'])
@login_required
def new_record():
	form = AddNewRecordForm()
	if form.validate_on_submit():

		# Checks if File Code is in database
		has_code = db.session.execute(sql_text.get_file_by_code(),{
			'code': form.code.data
			}).fetchone()
		
		if has_code != None:
			datetime = form.date.data + timedelta(hours=-8)
			files = request.files.getlist("attachment_files")
			
			new_images = []
			if files:
				folder_name = datetime.strftime('%m-%d-%Y')
				folder_path = 'uploads/'+folder_name
				folder_number=0
				while (glob.glob(os.path.join(app.instance_path, folder_path, str(folder_number)))):
					folder_number+=1
				file_name = 0
				ScanAndFixSameName(files)
				for file in files:
					if not allowed_file(file.filename):
						flash('Some of the files uploaded is not of the accepted format.', 'warning')
						return redirect(url_for("new_record"))

				new_document = Document(
					code=has_code.file_id,
					subject=form.subject.data, 
					sent_by=form.sender.data,
					received_by=form.receiver.data
					)
				db.session.add(new_document)
				db.session.commit()

				new_record = Record(
					file_id=has_code.file_id,
					document_id=new_document.doc_id,
					comm_type=form.record_type.data,
					datetime=datetime,
					processed_by=form.processed_by.data
					)
				db.session.add(new_record)
				db.session.commit()
				for file in files:
					os.makedirs(os.path.join(app.instance_path, folder_path, str(folder_number)), exist_ok=True)
					file.save(os.path.join(app.instance_path,
						folder_path,
						str(folder_number),
						secure_filename(file.filename)))
					
					new_record.images.append(Image(image_name=os.path.join(str(folder_number), file.filename), record_id=new_record.rec_id))
				db.session.commit()
			else:
				new_document = Document(
					has_code.file_id,
					form.subject.data, 
					form.sender.data,
					form.receiver.data
					)
				db.session.add(new_document)

				new_record = Record(
					has_code.file_id,
					new_document.doc_id,
					comm_type,
					datetime,
					form.processed_by.data
					)
				new_record.document.append(new_document)
				db.session.add(new_record)
				db.session.commit()
			db.session.commit()

			flash('New Record Has Been Added to the Database.', 'success')
			return redirect(url_for('view_logs'))
		else:
			flash("Error, No file code found in the database. Either check for spelling errors or add it from the main menu", 'warning')

	elif request.method == 'GET':
		form.date.data = form.date.data + timedelta(hours=8)
	return render_template('new_record.html', title="New Record", form=form, legend='New Record')


@app.route("/view_logs/manage_file_codes/<code_id>/modify", methods=['GET', 'POST'])
@login_required
def update_code(code_id):
	print(code_id)
	all_codes = db.session.execute(sql_text.get_codes()).fetchall()
	form = AddNewCodeForm()
	updating=False # Since the adding/modifing and viewing are done on the same page. This is just to let the HTML know whether or not the user is
				   # Updating a File code or creating a new one
	if form.validate_on_submit():
		has_code = db.session.execute(sql_text.get_file_by_code(),{ #Checks to see if the code entered by the user is already in the database
			'code': form.code.data
			}).fetchone()
		
		if has_code == None or code_id == has_code.code: # If the code is new, then continue with the new File Code Creation
			query = sql_text.update_file()
			db.session.execute(query, {'code':code_id,'new_code':re.sub('\s+',' ',form.code.data).strip(), 'name':form.name.data})
			db.session.commit()
			flash("Updated New Code Entry!", 'success')
			return redirect(url_for('manage_codes'))
		else: # Else Flash the message
			flash("That code is already being used, please use a different code", 'warning')
		

	elif request.method == 'GET': # If the user is updating a File code. We want the information of the File Code the user is modifying to be shown on the input fields by default
		codes = db.session.execute(sql_text.get_file_by_code(), {'code':code_id}).fetchone() # Which is what this does.
		form.code.data 	    	= codes.code
		form.name.data 	    	= codes.name
		form.submit.label.text = 'Update File Code'
		updating = True # Now that they're updating a file code. This is set to true
	return render_template('file_codes.html', codes=all_codes, title="Manage Codes", form=form, update=updating)


@app.route("/view_logs/<record_id>/modify", methods=['GET', 'POST'])
@login_required
def record_update(record_id):
	record  = db.session.execute(sql_text.get_record_by_id(),{'id':record_id}).fetchone()
	document= db.session.execute(sql_text.get_document_by_id(),{'id':record.document_id}).fetchone()
	form = AddNewRecordForm()

	if form.validate_on_submit():

		# Checks if File Code is in database
		has_code = db.session.execute(sql_text.get_file_by_code(),{
			'code': form.code.data
			}).fetchone()

		
		if has_code != None:
			datetime = form.date.data + timedelta(hours=-8)
			db.session.execute(sql_text.update_document(),{
				'code'   : form.code.data,
				'subject': form.subject.data,
				'id'	 : document.doc_id
				})
			db.session.commit()


			db.session.execute(sql_text.update_record(),{
				'code' : form.code.data,
				'doc_id': document.doc_id,
				'datetime':datetime,
				'comm_type': form.record_type.data,
				'processed_by':form.processed_by.data,
				'rec_id':record_id
				})
			db.session.commit()
			flash('New Record Has Been Updated!', 'success')
			return redirect(url_for('view_logs'))
		else:
			flash("Error, No file code found in the database. Either check for spelling errors or add it from the Code Management Page", 'warning')

	# This is when the user first enters the page
	# Instead of serving out an empty form for the user to update, the form fields are populated with data from the database
	elif request.method == 'GET':
		form.code.data 	    	= record.file_id
		form.subject.data   	= document.subject
		form.processed_by.data  = record.processed_by
		form.date.data 			= record.datetime + timedelta(hours=8)
		form.submit.label.text = 'Update'

	return render_template('/new_record.html',
		title='Modify Record',
		form=form,
		legend='Modify Record',
		btn_txt="Update")



# Login, Register, Logout, Account Page
# =====================================
@app.route("/settings")
@login_required
def settings():
	return render_template('settings.html')

@app.route("/manage_users")
@login_required
def manage_users():
	
	permissions = Permission.query.all()
	users 		= User.query.all()
	all_perms = []
	all_users = []
	for user in users:
		all_users.append(user)
	for perms in permissions:
		all_perms.append(perms)
	return render_template('page_system_accounts.html',
		perms=all_perms,
		users=all_users)

@app.route("/archives")
@login_required
def archives():
	queries = "%"+request.args.get('q', '', type=str)+"%" 		
	page 	= request.args.get('page', 1, type=int)				
	per_page= request.args.get('pp', 10, type=int)				
	sort_by = request.args.get('sb', 'all', type=str)		
	ord_by  = request.args.get('ob', 'desc', type=str)		
	show_type = request.args.get('sr', 'both',type=str)
	filter_records = []
	if show_type == "both":
		sent = 'sent'
		rec  = 'received'
	elif show_type == "sent":
		sent = 'sent'
		rec  = ''
	elif show_type == "rec":
		sent = ''
		rec  = 'received'
	earliest = db.session.execute(sql_text.get_earliest_date()).fetchone()
	latest = db.session.execute(sql_text.get_latest_date()).fetchone()
	current_time = datetime.utcnow() + timedelta(hours=8) 
	earliest = current_time if earliest[0] == None else earliest[0] + timedelta(hours=8)
	latest = current_time if latest[0] == None else latest[0] + timedelta(hours=8)

	before = request.args.get('db', latest.strftime('%Y-%m-%d'))
	after = request.args.get('da', earliest.strftime('%Y-%m-%d'))
	try :
		before = datetime.strptime(before+' 23:59:00','%Y-%m-%d %H:%M:%S')
		after = datetime.strptime(after+' 00:00:00','%Y-%m-%d %H:%M:%S')
	except: 
		before = latest.strftime('%Y-%m-%d')
		after = earliest.strftime('%Y-%m-%d')
		before = datetime.strptime(before+' 23:59:00','%Y-%m-%d %H:%M:%S')
		after = datetime.strptime(after+' 00:00:00','%Y-%m-%d %H:%M:%S')
		flash("Date Arguement Invalid. Try Reseting Search Options Before continuing", "warning")
	dates=(before, after)
	if dates[0] < dates[1]:
		flash("Filter Options Error, The first date must be older than the second", 'warning')
		dates=(latest, earliest)
	if per_page <= 0:
		per_page = 1 


	record_count=db.session.execute(sql_text.get_count(),{
		'table_name': 'records'									
		}).fetchone()
	record_count = record_count[0]
	
	posts=db.session.execute(sql_text.get_all_records(sort_by),{
		'query':queries,
		'date01': (dates[1]+ timedelta(hours=-8)).strftime("%Y-%m-%d 00:00"),
		'date02': (dates[0]+ timedelta(hours=-8)).strftime("%Y-%m-%d 23:59"),
		'sort_by':sort_by,
		'ord_by':ord_by,
		'sen':sent,
		'rec':rec,
		'offset':per_page*(page-1),
		'per_page':per_page,
		'deleted':1
		}).fetchall()
	print(per_page*(page-1))
	print(per_page)
	
	all_posts = []	
	for post in posts:
		if post != None:
			post = dict(post)
			post['datetime'] = post['datetime'] + timedelta(hours=8)
			post['datetime'] = post['datetime'].strftime("%B %d, %Y %I:%M%p")
			all_posts.append(post)
	#==================================
	pagination = Pagination(page, per_page, record_count)		
	return render_template('archives.html',
		post=all_posts,
		pp=per_page,
		page=page,
		sb=sort_by,
		ob=ord_by,
		record_count=record_count,
		pagination= pagination,
		show_type=show_type,
		dates=dates)

@app.route("/logout")
@login_required
def logout():
	logout_user()
	return redirect(url_for('login'))

@app.route("/login",  methods=['GET', 'POST'])
def login():
	form = LoginForm()
	if current_user.is_authenticated:
		return redirect(url_for('view_logs'))
	if form.validate_on_submit():
		user = User.query.filter_by(username=form.username.data).first()
		print(user.username)
		print(form.username.data)
		if user and bcrypt.check_password_hash(user.password, form.password.data):
			login_user(user)
			next_page = request.args.get('next')
			if next_page == "logout":
				next_page = "view_logs"
			return redirect(next_page) if next_page else redirect(url_for('view_logs'))
		else:
			flash('Login Failed. Check Username or Password','danger')
	return render_template('login.html', title = 'login', form=form)

@login_required
@app.route("/<username>/change_password", methods=["GET","POST"])
def change_password(username):
	form = ChangePasswordForm(username=username)
	if form.validate_on_submit():
		hashed_password = bcrypt.generate_password_hash(form.password.data)
		user = User.query.filter_by(username=form.username.data).first()
		user.password = hashed_password
		db.session.commit()
		return redirect("manage_users")
	return render_template('change_password.html', form=form, username=username)

@app.route("/register", methods=['GET', 'POST'])
def register():
	if current_user.is_authenticated:
		return redirect(url_for('view_logs'))
	form = RegisterationForm()
	if form.validate_on_submit():

		hashed_password = bcrypt.generate_password_hash(form.password.data)
		user = User(
			username=form.username.data, 
			full_name=form.full_name.data,
			password=hashed_password
			)
		db.session.add(user)
		db.session.commit()
		flash('Account Created! You may now log in!', 'success')
		return redirect(url_for('login'))
	return render_template('register.html', title = 'Register', form=form)


#Helper Functions
UPLOAD_FOLDER = '/uploads'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS