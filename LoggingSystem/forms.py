from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, TextAreaField, SelectField, TimeField, DateField, DateTimeField, IntegerField, FileField, HiddenField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from LoggingSystem.models import User
from datetime import datetime, timedelta

class RegisterationForm(FlaskForm):
	username = StringField('Username', validators=[DataRequired(), Length(min = 2, max = 20)])
	full_name = StringField('Full Name', validators=[DataRequired()])
	password = PasswordField('Password', validators=[DataRequired()])
	password_confirm = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
	submit = SubmitField('Create Account')
	def validate_username(self, username):
		username = User.query.filter_by(username=username.data).first()
		if username:
			raise ValidationError('Error: That Username is already taken. ')
			

class LoginForm(FlaskForm):
	username = StringField('Username', validators=[DataRequired()])
	password = PasswordField('Password', validators=[DataRequired()])
	remember = BooleanField("Remember Me")
	submit = SubmitField('Login')


class ModifyPermissionForm(FlaskForm):
	permissions = SelectField('Permissions', choices=[])
	submit = SubmitField("Update")

class ChangePasswordForm(FlaskForm):
	username = HiddenField("")
	password = PasswordField('New Password', validators=[DataRequired()])
	password_confirm = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
	submit   = SubmitField("Update")


class AddNewCodeForm(FlaskForm):
	code = StringField('File Code', validators=[DataRequired()])
	name = StringField('Information', validators=[DataRequired()])
	submit = SubmitField('Add File Code')	

class AddNewRecordForm(FlaskForm):
	# Client Code 
	date 		= DateTimeField('Date', format="%m-%d-%Y %H:%M", default=datetime.utcnow, validators=[DataRequired("Please Use The Format Stated Above For The Date!")])
	subject 	= StringField('Subject ', validators=[DataRequired()])
	sender = StringField('Document Sender', validators=[DataRequired()])
	receiver = StringField('Document Receiver', validators=[])
	processed_by = StringField('Processed By', validators=[DataRequired()])
	code 		= StringField('File Code', validators=[DataRequired()])
	#doc_image_file = FileField(validators=[])
	attachment_files = FileField(validators=[])
	record_type = SelectField('Sent/Recieved', 
		choices=[('received', 'Received'),('sent', 'Sent')],
		validators=[DataRequired()])
	submit = SubmitField('Add Record')
