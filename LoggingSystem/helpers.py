from math import ceil
from sqlalchemy import inspect

def row2dict(row):
    d = {}
    for column in row.__table__.columns:
        d[column.name] = str(getattr(row, column.name))

    return d

def ScanAndFixSameName(files):
    file_name_list = []
    for file in files:
        if not file.filename in file_name_list:
            file_name_list.append(file.filename)
        else:
            i = 0
            while True:
                file_name_split = file.filename.split(".")
                file_name_split.pop()
                file_name = '.'.join(file_name_split)
                new_name = file_name+'('+str(i)+')'+file.filename.split(".")[-1]
                if not new_name in file_name_list:
                    file_name_list.append(file.filename)
                    file.filename = new_name
                    break
                else:
                    i += 1
    return True

class Pagination(object):

    def __init__(self, page, per_page, total_count):
        self.page = page
        self.per_page = per_page
        self.total_count = total_count

    @property
    def pages(self):
        return int(ceil(self.total_count / float(self.per_page)))

    @property
    def has_prev(self):
        return self.page > 1

    @property
    def has_next(self):
        return self.page < self.pages

    def iter_pages(self, left_edge=2, left_current=2,
                   right_current=5, right_edge=2):
        last = 0
        for num in range(1, self.pages + 1):
            if num <= left_edge or \
               (num > self.page - left_current - 1 and \
                num < self.page + right_current) or \
               num > self.pages - right_edge:
                if last + 1 != num:
                    yield None
                yield num
                last = num