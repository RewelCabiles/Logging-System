from LoggingSystem import db, login_manager, app
from flask_login import UserMixin

if app.db_type == "mssql":
	from sqlalchemy.dialects.mssql import BIT
else:
	from sqlalchemy.dialects.mysql import BIT

@login_manager.user_loader
def load_user(user_id):
	return User.query.get(user_id)

perm_user_assoc = db.Table(
    'user permissions',
    db.Column('user_id', db.Integer, db.ForeignKey('user.user_id')),
    db.Column('permission_id', db.Integer, db.ForeignKey('permission.permission_id'))
)

class User(db.Model, UserMixin):
	user_id         = db.Column(db.Integer	, primary_key = True)
	username        = db.Column(db.VARCHAR(20), unique = True, nullable = False)
	password        = db.Column(db.VARCHAR(80), nullable = False)
	full_name       = db.Column(db.VARCHAR(20), nullable = False)
	permission 		= db.relationship("Permission", secondary=perm_user_assoc, back_populates="user")
	def get_id(self):
		return self.user_id


class Permission(db.Model):
	permission_id   = db.Column(db.Integer, primary_key = True)
	name 		    = db.Column(db.VARCHAR(40), nullable = False)
	description 	= db.Column(db.VARCHAR(100), nullable = True)
	user    		= db.relationship("User", secondary=perm_user_assoc, back_populates="permission")


class File(db.Model):
	file_id			= db.Column(db.Integer      , primary_key = True)
	code 			= db.Column(db.VARCHAR(20)  , nullable=False)
	name 			= db.Column(db.VARCHAR(40)  , nullable = False)
	documents   	= db.relationship('Document', lazy=True)
	records			= db.relationship("Record"  , uselist=False, back_populates="file")
	deleted 		= db.Column(BIT()    , nullable = False, default=0)

class Document(db.Model):
	doc_id   		= db.Column(db.Integer     , primary_key = True)
	code 	 	    = db.Column(db.Integer , db.ForeignKey('file.file_id'), nullable = False)
	subject 	 	= db.Column(db.VARCHAR(100), nullable = False)
	records			= db.relationship("Record"    , uselist=False, back_populates="document")
	sent_by			= db.Column(db.VARCHAR(50), nullable = False)
	received_by		= db.Column(db.VARCHAR(50), nullable = False)
	deleted 		= db.Column(BIT()  , nullable = False, default=0)

class Record(db.Model):
	rec_id			= db.Column(db.Integer      ,primary_key = True)
	file_id 		= db.Column(db.Integer      ,db.ForeignKey('file.file_id'), nullable = False)
	document_id 	= db.Column(db.Integer      ,db.ForeignKey('document.doc_id'), nullable = False)
	file 			= db.relationship("File"    ,back_populates="records")
	document 		= db.relationship("Document",back_populates="records")
	images 			= db.relationship("Image"   ,backref="records")
	comm_type 	 	= db.Column(db.VARCHAR(20)  ,nullable = False)
	datetime		= db.Column(db.DateTime     ,nullable = False)
	processed_by	= db.Column(db.VARCHAR(40)  ,nullable = False)
	deleted 		= db.Column(BIT()  , nullable = True, default=0)

class Image(db.Model):
	image_id		= db.Column(db.Integer, primary_key = True)
	image_name		= db.Column(db.VARCHAR(60), nullable = False)
	record_id		= db.Column(db.Integer, db.ForeignKey("record.rec_id"), nullable = False)
	