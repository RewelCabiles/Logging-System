##Document Logging System

***Information***

This Web-Apps is a Logging System that allows users to log incoming and outgoing documents.

 When the program is running, it launches a **web-server** which allows users to access the actual **app** itself via a website on the browser.

If the server is running on the same computer, then the **URL** to input in the web-browser is simply `localhost:5000`


***Dependencies***
(Anywhere it is mentioned that code should be typed, the code should be typed inside *cmd* for windows, or any *terminal* on linux)

 -Python3
 -python3-pip (Usually installed by default with Python3 on windows, on linux install with `sudo apt install python3-pip`)
 -virtualenvi (Installed using pip, type `pip3 install virtualenv` to install)

***Database Options***
By default windows installations of this has the app configured to use *MSSQL* On linux, it uses *MySQL*

=== First Time Setup
	run SETUP.bat
=== Running
	double click START.bat
